class User:
    def __init__(self, id, email, name, password):
        self.id = id
        self.email = email
        self.name = name
        self.password = password

    def __str__(self):
        return self.id + ";" + self.email + ";" + self.name + ";" + self.password