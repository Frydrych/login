import hashlib
import uuid
from Login import User


def is_email_in_file(email):
    file = open("users.txt", "r")
    for line in file:
        splited_line = line.strip().split(";")
        temp_email = splited_line[1]
        if temp_email == email:
            file.close()
            return True
    file.close()
    return False


def registration():

    id = str(uuid.uuid4())
    email = input("Podaj email:")
    name = input("Podaj name:")
    a = input("Podaj hasło:")
    b = input("Podaj ponownie hasło:")
    password = hashlib.md5(a.encode('utf-8')).hexdigest()
    password_chceck = hashlib.md5(b.encode('utf-8')).hexdigest()
    user = User(id, email, name, password)

    if is_email_in_file(email):
        print("Użytkownik o podamym adresie email istenieje")
        menu()


    if a == b:
        file = open("users.txt", "a")
        file.write(str(user) + "\n")
        file.close()
        print("Dodano użytkownika:")
        print(user)

    else:
        print("Hasła różnią się")

    menu()


def login():

    email = input("Podaj email:")
    password = input("Podaj hasło:")
    hashed_password = hashlib.md5(password.encode('utf-8')).hexdigest()

    is_found = False
    file = open("users.txt", "r")
    for line in file:
        splited_line = line.strip().split(";")
        temp_email = splited_line[1]
        temp_username = splited_line[2]
        temp_password = splited_line[3]
        if email == temp_email and temp_password == hashed_password:
            print("witaj ", temp_username)
            is_found = True

    file.close()
    if is_found == False:
     print("Nie znaleziono uzytkownika z podanymi danymi logowania")

    menu()

def users_list():
    file = open("users.txt", "r")
    for line in file:
        splited_line = line.strip().split(";")
        temp_email = splited_line[1]
        temp_username = splited_line[2]
        temp_id = splited_line[0]
        print("id: " + temp_id + ", email: " + temp_email + ", imie: " + temp_username)
    file.close()

    menu()



def menu():
    print("""
======================MENU========================

1. Zarejestruj się
2. Zaloguj się
3. Lista użytkowników
""")
    choise = int(input())
    if (choise == 1):
        registration()
    if (choise == 2):
        login()
    if (choise == 3):
        users_list()

menu()